const lp1_pdbs_M = {
  '0:0/0': 0n,
  '0:1/1': 1n,
  '1:1/0': 2n,
  '4:0/1': 3n,
  '1:3/0': 4n,
  '7:0/1': 5n,
  '4:0/5': 6n,
  '1:6/0': 7n,
  '2:0/1': 8n,
  '2:0/6': 9n,
  '1:9/0': 10n,
  '5:0/1': 11n,
  '5:0/5': 12n,
  '4:0/12': 13n,
  '2:0/13': 14n,
  '1:14/0': 15n,
  '8:0/1': 16n,
  '7:0/16': 17n,
  '5:0/17': 18n,
  '4:0/18': 19n,
  '2:0/19': 20n,
  '1:20/0': 21n,
  '3:0/1': 22n,
  '3:0/19': 23n,
  '2:0/23': 24n,
  '1:24/0': 25n,
  '6:0/1': 26n,
  '6:0/17': 27n,
  '5:0/27': 28n,
  '4:0/28': 29n,
  '3:0/29': 30n,
  '2:0/30': 31n,
  '1:31/0': 32n,
  '9:0/1': 33n,
  '8:0/33': 34n,
  '7:0/34': 35n,
  '6:0/35': 36n,
  '5:0/36': 37n,
  '4:0/37': 38n,
  '3:0/38': 39n,
  '2:0/39': 40n,
  '1:40/0': 41n,
  '1:0/1': 42n,
  '4:1/0': 43n,
  '1:0/43': 44n,
  '4:5/0': 45n,
  '1:0/45': 46n,
  '2:1/0': 47n,
  '2:45/0': 48n,
  '1:0/48': 49n,
  '5:1/0': 50n,
  '5:5/0': 51n,
  '4:51/0': 52n,
  '2:52/0': 53n,
  '1:0/53': 54n,
  '5:17/0': 55n,
  '4:55/0': 56n,
  '2:56/0': 57n,
  '1:0/57': 58n,
  '3:0/56': 59n,
  '2:59/0': 60n,
  '1:0/60': 61n,
  '5:27/0': 62n,
  '4:62/0': 63n,
  '3:0/63': 64n,
  '2:64/0': 65n,
  '1:0/65': 66n,
  '9:1/0': 67n,
  '8:0/67': 68n,
  '7:0/68': 69n,
  '6:0/69': 70n,
  '5:70/0': 71n,
  '4:71/0': 72n,
  '3:0/72': 73n,
  '2:73/0': 74n,
  '1:0/74': 75n,
  '1:40/74': 76n };

const lp1_pprog_M = {
  '0:0/0': 0n,
  '0:1/1': 1n,
  '1:0/1': 2n,
  '4:1/0': 3n,
  '1:0/3': 4n,
  '7:0/1': 5n,
  '4:5/0': 6n,
  '1:0/6': 7n,
  '2:0/1': 8n,
  '5:0/1': 9n,
  '2:0/9': 10n,
  '8:0/1': 11n,
  '5:0/11': 12n,
  '2:0/12': 13n,
  '3:0/1': 14n,
  '6:0/1': 15n,
  '3:0/15': 16n,
  '9:0/1': 17n,
  '6:0/17': 18n,
  '3:0/18': 19n,
  '8:17/1': 20n,
  '6:11/20': 21n,
  '5:18/21': 22n,
  '3:12/22': 23n,
  '2:19/23': 24n,
  '10:0/1': 25n,
  '1:0/25': 26n,
  '1:1/0': 27n,
  '10:1/0': 28n,
  '1:28/0': 29n,
  '1:28/25': 30n,
  '4:0/1': 31n,
  '13:0/1': 32n,
  '4:0/32': 33n,
  '13:1/0': 34n,
  '4:34/0': 35n,
  '4:34/32': 36n,
  '10:34/0': 37n,
  '10:32/0': 38n,
  '4:37/38': 39n,
  '10:0/34': 40n,
  '10:0/32': 41n,
  '4:40/41': 42n,
  '1:39/42': 43n,
  '16:0/1': 44n,
  '7:0/44': 45n,
  '7:1/0': 46n,
  '16:1/0': 47n,
  '7:47/0': 48n,
  '7:47/44': 49n,
  '13:47/0': 50n,
  '10:50/0': 51n,
  '13:44/0': 52n,
  '10:52/0': 53n,
  '7:51/53': 54n,
  '13:0/47': 55n,
  '10:55/0': 56n,
  '13:0/44': 57n,
  '10:57/0': 58n,
  '7:56/58': 59n,
  '4:54/59': 60n,
  '10:0/50': 61n,
  '10:0/52': 62n,
  '7:61/62': 63n,
  '10:0/55': 64n,
  '10:0/57': 65n,
  '7:64/65': 66n,
  '4:63/66': 67n,
  '1:60/67': 68n,
  '11:0/1': 69n,
  '3:0/69': 70n,
  '3:1/0': 71n,
  '11:1/0': 72n,
  '3:72/0': 73n,
  '3:72/69': 74n,
  '11:50/0': 75n,
  '10:75/0': 76n,
  '11:52/0': 77n,
  '10:77/0': 78n,
  '7:76/78': 79n,
  '11:55/0': 80n,
  '10:80/0': 81n,
  '11:57/0': 82n,
  '10:82/0': 83n,
  '7:81/83': 84n,
  '4:79/84': 85n,
  '11:0/50': 86n,
  '10:86/0': 87n,
  '11:0/52': 88n,
  '10:88/0': 89n,
  '7:87/89': 90n,
  '11:0/55': 91n,
  '10:91/0': 92n,
  '11:0/57': 93n,
  '10:93/0': 94n,
  '7:92/94': 95n,
  '4:90/95': 96n,
  '3:85/96': 97n,
  '10:0/75': 98n,
  '10:0/77': 99n,
  '7:98/99': 100n,
  '10:0/80': 101n,
  '10:0/82': 102n,
  '7:101/102': 103n,
  '4:100/103': 104n,
  '10:0/86': 105n,
  '10:0/88': 106n,
  '7:105/106': 107n,
  '10:0/91': 108n,
  '10:0/93': 109n,
  '7:108/109': 110n,
  '4:107/110': 111n,
  '3:104/111': 112n,
  '1:97/112': 113n,
  '14:0/1': 114n,
  '6:0/114': 115n,
  '6:1/0': 116n,
  '14:1/0': 117n,
  '6:117/0': 118n,
  '6:117/114': 119n,
  '14:47/0': 120n,
  '13:120/0': 121n,
  '11:121/0': 122n,
  '10:122/0': 123n,
  '14:44/0': 124n,
  '13:124/0': 125n,
  '11:125/0': 126n,
  '10:126/0': 127n,
  '7:123/127': 128n,
  '14:0/47': 129n,
  '13:129/0': 130n,
  '11:130/0': 131n,
  '10:131/0': 132n,
  '14:0/44': 133n,
  '13:133/0': 134n,
  '11:134/0': 135n,
  '10:135/0': 136n,
  '7:132/136': 137n,
  '6:128/137': 138n,
  '13:0/120': 139n,
  '11:139/0': 140n,
  '10:140/0': 141n,
  '13:0/124': 142n,
  '11:142/0': 143n,
  '10:143/0': 144n,
  '7:141/144': 145n,
  '13:0/129': 146n,
  '11:146/0': 147n,
  '10:147/0': 148n,
  '13:0/133': 149n,
  '11:149/0': 150n,
  '10:150/0': 151n,
  '7:148/151': 152n,
  '6:145/152': 153n,
  '4:138/153': 154n,
  '11:0/121': 155n,
  '10:155/0': 156n,
  '11:0/125': 157n,
  '10:157/0': 158n,
  '7:156/158': 159n,
  '11:0/130': 160n,
  '10:160/0': 161n,
  '11:0/134': 162n,
  '10:162/0': 163n,
  '7:161/163': 164n,
  '6:159/164': 165n,
  '11:0/139': 166n,
  '10:166/0': 167n,
  '11:0/142': 168n,
  '10:168/0': 169n,
  '7:167/169': 170n,
  '11:0/146': 171n,
  '10:171/0': 172n,
  '11:0/149': 173n,
  '10:173/0': 174n,
  '7:172/174': 175n,
  '6:170/175': 176n,
  '4:165/176': 177n,
  '3:154/177': 178n,
  '10:0/122': 179n,
  '10:0/126': 180n,
  '7:179/180': 181n,
  '10:0/131': 182n,
  '10:0/135': 183n,
  '7:182/183': 184n,
  '6:181/184': 185n,
  '10:0/140': 186n,
  '10:0/143': 187n,
  '7:186/187': 188n,
  '10:0/147': 189n,
  '10:0/150': 190n,
  '7:189/190': 191n,
  '6:188/191': 192n,
  '4:185/192': 193n,
  '10:0/155': 194n,
  '10:0/157': 195n,
  '7:194/195': 196n,
  '10:0/160': 197n,
  '10:0/162': 198n,
  '7:197/198': 199n,
  '6:196/199': 200n,
  '10:0/166': 201n,
  '10:0/168': 202n,
  '7:201/202': 203n,
  '10:0/171': 204n,
  '10:0/173': 205n,
  '7:204/205': 206n,
  '6:203/206': 207n,
  '4:200/207': 208n,
  '3:193/208': 209n,
  '1:178/209': 210n,
  '17:0/1': 211n,
  '9:0/211': 212n,
  '9:1/0': 213n,
  '17:1/0': 214n,
  '9:214/0': 215n,
  '9:214/211': 216n,
  '16:214/0': 217n,
  '14:217/0': 218n,
  '13:218/0': 219n,
  '11:219/0': 220n,
  '10:220/0': 221n,
  '16:211/0': 222n,
  '14:222/0': 223n,
  '13:223/0': 224n,
  '11:224/0': 225n,
  '10:225/0': 226n,
  '9:221/226': 227n,
  '16:0/214': 228n,
  '14:228/0': 229n,
  '13:229/0': 230n,
  '11:230/0': 231n,
  '10:231/0': 232n,
  '16:0/211': 233n,
  '14:233/0': 234n,
  '13:234/0': 235n,
  '11:235/0': 236n,
  '10:236/0': 237n,
  '9:232/237': 238n,
  '7:227/238': 239n,
  '14:0/217': 240n,
  '13:240/0': 241n,
  '11:241/0': 242n,
  '10:242/0': 243n,
  '14:0/222': 244n,
  '13:244/0': 245n,
  '11:245/0': 246n,
  '10:246/0': 247n,
  '9:243/247': 248n,
  '14:0/228': 249n,
  '13:249/0': 250n,
  '11:250/0': 251n,
  '10:251/0': 252n,
  '14:0/233': 253n,
  '13:253/0': 254n,
  '11:254/0': 255n,
  '10:255/0': 256n,
  '9:252/256': 257n,
  '7:248/257': 258n,
  '6:239/258': 259n,
  '13:0/218': 260n,
  '11:260/0': 261n,
  '10:261/0': 262n,
  '13:0/223': 263n,
  '11:263/0': 264n,
  '10:264/0': 265n,
  '9:262/265': 266n,
  '13:0/229': 267n,
  '11:267/0': 268n,
  '10:268/0': 269n,
  '13:0/234': 270n,
  '11:270/0': 271n,
  '10:271/0': 272n,
  '9:269/272': 273n,
  '7:266/273': 274n,
  '13:0/240': 275n,
  '11:275/0': 276n,
  '10:276/0': 277n,
  '13:0/244': 278n,
  '11:278/0': 279n,
  '10:279/0': 280n,
  '9:277/280': 281n,
  '13:0/249': 282n,
  '11:282/0': 283n,
  '10:283/0': 284n,
  '13:0/253': 285n,
  '11:285/0': 286n,
  '10:286/0': 287n,
  '9:284/287': 288n,
  '7:281/288': 289n,
  '6:274/289': 290n,
  '4:259/290': 291n,
  '11:0/219': 292n,
  '10:292/0': 293n,
  '11:0/224': 294n,
  '10:294/0': 295n,
  '9:293/295': 296n,
  '11:0/230': 297n,
  '10:297/0': 298n,
  '11:0/235': 299n,
  '10:299/0': 300n,
  '9:298/300': 301n,
  '7:296/301': 302n,
  '11:0/241': 303n,
  '10:303/0': 304n,
  '11:0/245': 305n,
  '10:305/0': 306n,
  '9:304/306': 307n,
  '11:0/250': 308n,
  '10:308/0': 309n,
  '11:0/254': 310n,
  '10:310/0': 311n,
  '9:309/311': 312n,
  '7:307/312': 313n,
  '6:302/313': 314n,
  '11:0/260': 315n,
  '10:315/0': 316n,
  '11:0/263': 317n,
  '10:317/0': 318n,
  '9:316/318': 319n,
  '11:0/267': 320n,
  '10:320/0': 321n,
  '11:0/270': 322n,
  '10:322/0': 323n,
  '9:321/323': 324n,
  '7:319/324': 325n,
  '11:0/275': 326n,
  '10:326/0': 327n,
  '11:0/278': 328n,
  '10:328/0': 329n,
  '9:327/329': 330n,
  '11:0/282': 331n,
  '10:331/0': 332n,
  '11:0/285': 333n,
  '10:333/0': 334n,
  '9:332/334': 335n,
  '7:330/335': 336n,
  '6:325/336': 337n,
  '4:314/337': 338n,
  '3:291/338': 339n,
  '10:0/220': 340n,
  '10:0/225': 341n,
  '9:340/341': 342n,
  '10:0/231': 343n,
  '10:0/236': 344n,
  '9:343/344': 345n,
  '7:342/345': 346n,
  '10:0/242': 347n,
  '10:0/246': 348n,
  '9:347/348': 349n,
  '10:0/251': 350n,
  '10:0/255': 351n,
  '9:350/351': 352n,
  '7:349/352': 353n,
  '6:346/353': 354n,
  '10:0/261': 355n,
  '10:0/264': 356n,
  '9:355/356': 357n,
  '10:0/268': 358n,
  '10:0/271': 359n,
  '9:358/359': 360n,
  '7:357/360': 361n,
  '10:0/276': 362n,
  '10:0/279': 363n,
  '9:362/363': 364n,
  '10:0/283': 365n,
  '10:0/286': 366n,
  '9:365/366': 367n,
  '7:364/367': 368n,
  '6:361/368': 369n,
  '4:354/369': 370n,
  '10:0/292': 371n,
  '10:0/294': 372n,
  '9:371/372': 373n,
  '10:0/297': 374n,
  '10:0/299': 375n,
  '9:374/375': 376n,
  '7:373/376': 377n,
  '10:0/303': 378n,
  '10:0/305': 379n,
  '9:378/379': 380n,
  '10:0/308': 381n,
  '10:0/310': 382n,
  '9:381/382': 383n,
  '7:380/383': 384n,
  '6:377/384': 385n,
  '10:0/315': 386n,
  '10:0/317': 387n,
  '9:386/387': 388n,
  '10:0/320': 389n,
  '10:0/322': 390n,
  '9:389/390': 391n,
  '7:388/391': 392n,
  '10:0/326': 393n,
  '10:0/328': 394n,
  '9:393/394': 395n,
  '10:0/331': 396n,
  '10:0/333': 397n,
  '9:396/397': 398n,
  '7:395/398': 399n,
  '6:392/399': 400n,
  '4:385/400': 401n,
  '3:370/401': 402n,
  '1:339/402': 403n,
  '12:0/1': 404n,
  '15:0/1': 405n,
  '12:0/405': 406n,
  '18:0/1': 407n,
  '15:0/407': 408n,
  '12:0/408': 409n,
  '9:409/1': 410n,
  '6:409/410': 411n,
  '3:409/411': 412n,
  '8:409/1': 413n,
  '5:409/413': 414n,
  '8:410/1': 415n,
  '6:413/415': 416n,
  '5:411/416': 417n,
  '3:414/417': 418n,
  '2:412/418': 419n,
  '7:0/345': 420n,
  '7:0/352': 421n,
  '6:420/421': 422n,
  '4:422/0': 423n,
  '7:0/376': 424n,
  '7:0/383': 425n,
  '6:424/425': 426n,
  '4:426/0': 427n,
  '3:423/427': 428n,
  '1:0/428': 429n,
  '18:1/0': 430n,
  '17:430/0': 431n,
  '16:0/431': 432n,
  '15:228/432': 433n,
  '14:433/0': 434n,
  '13:434/0': 435n,
  '12:230/435': 436n,
  '11:436/0': 437n,
  '10:0/437': 438n,
  '17:0/430': 439n,
  '16:0/439': 440n,
  '15:233/440': 441n,
  '14:441/0': 442n,
  '13:442/0': 443n,
  '12:235/443': 444n,
  '11:444/0': 445n,
  '10:0/445': 446n,
  '9:438/446': 447n,
  '7:0/447': 448n,
  '14:0/433': 449n,
  '13:449/0': 450n,
  '12:250/450': 451n,
  '11:451/0': 452n,
  '10:0/452': 453n,
  '14:0/441': 454n,
  '13:454/0': 455n,
  '12:254/455': 456n,
  '11:456/0': 457n,
  '10:0/457': 458n,
  '9:453/458': 459n,
  '7:0/459': 460n,
  '6:448/460': 461n,
  '4:461/0': 462n,
  '11:0/436': 463n,
  '10:0/463': 464n,
  '11:0/444': 465n,
  '10:0/465': 466n,
  '9:464/466': 467n,
  '7:0/467': 468n,
  '11:0/451': 469n,
  '10:0/469': 470n,
  '9:470/0': 471n,
  '7:0/471': 472n,
  '6:468/472': 473n,
  '4:473/0': 474n,
  '3:462/474': 475n,
  '8:447/0': 476n,
  '7:0/476': 477n,
  '8:459/0': 478n,
  '7:0/478': 479n,
  '6:477/479': 480n,
  '5:461/480': 481n,
  '4:481/0': 482n,
  '8:467/0': 483n,
  '7:0/483': 484n,
  '8:471/0': 485n,
  '7:0/485': 486n,
  '6:484/486': 487n,
  '5:473/487': 488n,
  '4:488/0': 489n,
  '3:482/489': 490n,
  '2:475/490': 491n,
  '1:0/491': 492n };

const lp1_rules = [ {
  hsym: 7n,
  hasnegs: false,
  poss: {
    h: 492n,
    w: 2,
    x:
    [ true,false,true,true,false,true,true,false,true,true,true,false,
      true,true,false,true,true,false,false,false,false,false,false,
      false,false,false,false,false,false,false,false,false,false,false,
      false,false,false,false,false,false,false,false,false,false,false,
      false,false,false,false,false ],
    hvars:
    [ 0,1,2,3,4,5,6,7,8,9,10,2,12,13,5,15,16,8,18,19,20,21,22,23,24,25,26,
      27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49 ],
    bits: 3,
    ar: 3 },
  negs: {
    h: 1n,
    w: 0,
    x:
    [ false,false,false,false,false,false,false,false,false,false,false,
      false,false,false,false,false,false,false,false,false ],
    hvars:
    [ 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19 ],
    bits: 3,
    ar: 3 },
  neg: false } ];

module.exports = {
  lp1_pdbs_M, lp1_pprog_M, lp1_rules
}
